import 'package:get/get.dart';

import '../controllers/mobileotp_controller.dart';

class MobileotpBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MobileotpController>(
      () => MobileotpController(),
    );
  }
}
