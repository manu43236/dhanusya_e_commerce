import 'package:dhenusya_e_commerce/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../controllers/mobileotp_controller.dart';

class MobileotpView extends GetView<MobileotpController> {
  const MobileotpView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Align(
          alignment: Alignment.topCenter,
          child: Padding(
            padding: const EdgeInsets.only(top: 70.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.asset('assets/images/dhenusya_logo.png', width: 150),
                const SizedBox(height: 20),
                const Text(
                  'Dhenusya Organics',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.green, fontSize: 35),
                ),
                const SizedBox(height: 50),
                Image.asset('assets/images/phone_logo.png', width: 60),
                const SizedBox(height: 30),
                const Text(
                  ' Your OTP for 7731994990 is 4655',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
                const SizedBox(height: 40),
                const Text(
                  'validate Your OTP',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 22,
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(height:15),
                 
                Container(
                          width: 100,
                          height: 50,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.digitsOnly,
                              LengthLimitingTextInputFormatter(4)
                            ],
                            // controller: mobilecontroller,
                            // onChanged: (value) {
                            //   setState(() {
                            //     isValidMobilenumber = value.length == 10;
                            //   });
                            //   // Further validation here if required
                            // },
                            decoration: const InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              
                              hintText: '',
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Color.fromARGB(255, 141, 139, 139),),
                                
                                
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                                
                              ),
                            ),
                          ),
                        ),

                        const SizedBox(height:35),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(12.0),
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(
                          Radius.elliptical(100.0, 150.0),
                        ),
                      ),
                      child: const Text(
                        'Cancel',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                    ),
                      // const SizedBox(width:5), 
                    Container(
                      padding: const EdgeInsets.all(12.0),
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(
                          Radius.elliptical(100.0, 150.0),
                        ),
                      ),
                      child: const Text(
                        'Submit',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                    ),
                    // Container(
                    //   padding: const EdgeInsets.all(12.0),
                    //   decoration: BoxDecoration(
                    //     color: Colors.green,
                    //     borderRadius: BorderRadius.all(
                    //       Radius.elliptical(100.0, 150.0),
                    //     ),
                    //   ),
                    //   child: TextButton(
                    //     onPressed: () {
                    //       // Add your cancel button logic here
                    //     },
                    //     child: const Text(
                    //       'Cancel',
                    //       textAlign: TextAlign.center,
                    //       style: TextStyle(
                    //         color: Colors.white,
                    //         // decoration: TextDecoration.underline,
                    //         decorationColor: Colors.blue,
                    //         fontSize: 20,
                    //       ),
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
                const SizedBox(height:40),

                 TextButton(
                    onPressed: () {
                      Get.toNamed(Routes.MOBILENUMBER);
                      // Get.toNamed(Routes.MOBILEOTP);
                    },
                    child: const Text(
                      'Go Back',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 25,
                      ),
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
