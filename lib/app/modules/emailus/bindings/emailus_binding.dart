import 'package:get/get.dart';

import '../controllers/emailus_controller.dart';

class EmailusBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EmailusController>(
      () => EmailusController(),
    );
  }
}
