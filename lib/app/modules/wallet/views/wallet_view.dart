import 'package:dhenusya_e_commerce/app/routes/app_pages.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/wallet_controller.dart';

class WalletView extends GetView<WalletController> {
  const WalletView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('WalletView'),
      //   centerTitle: true,
      // ),
      body: Container(
        padding: EdgeInsets.only(top: 60), // Adjust the top padding
        child: SingleChildScrollView(
          child: Container(
            alignment: Alignment.topCenter,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image.asset(
                'assets/images/dhenusya_logo.png',
                height: 150,
                width: 150,
                fit: BoxFit.cover,
              ),
              const SizedBox(height: 10),
              const Text(
                'Dhenusya Organics',
                style: TextStyle(
                  fontSize: 28,
                  color: Color.fromRGBO(12, 96, 30, 1),
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 15),
              const Text(
                'Welcome ,',
                style: TextStyle(
                  fontSize: 22,
                  color: Color.fromARGB(255, 0, 10, 1),
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 20),
              ElevatedButton(
                onPressed: () {
                  // Handle Sign Up with Google action
                },
                style: ElevatedButton.styleFrom(
                  primary: Color.fromRGBO(193, 192, 191, 1),
                  minimumSize: Size(260, 45),
                ),
                child: const Text(
                  'Your Wallet Available Balance: ₹ xxxxx',
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(height: 18),
              ElevatedButton(
                onPressed: () {
                  // Handle navigation to the Cartorders page
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //     builder: (context) => Cartorders(),
                  //   ),
                  // );
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.orange,
                  minimumSize: Size(235, 41),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(13.0),
                  ),
                ),
                child: const Text(
                  'Track Cart Orders',
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(height: 7),
              ElevatedButton(
                onPressed: () {
                  // Handle navigation to the Subscription page
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //     builder: (context) => Subscription(),
                  //   ),
                  // );
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.orange,
                  minimumSize: Size(235, 41),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(13.0),
                  ),
                ),
                child: const Text(
                  'Track Subscription orders',
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(height: 7),
              ElevatedButton(
                onPressed: () {
                  // Handle navigation to the homepage
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //     builder: (context) => homepage(),
                  //   ),
                  // );
                  Get.toNamed(Routes.HOMEPAGE);
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.orange,
                  minimumSize: Size(235, 41),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(13.0),
                  ),
                ),
                child: const Text(
                  'Continue Shopping',
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      )
    );
  }
}

