import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/user_register_controller.dart';

class UserRegisterView extends GetView<UserRegisterController> {
  const UserRegisterView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('UserRegisterView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'UserRegisterView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
