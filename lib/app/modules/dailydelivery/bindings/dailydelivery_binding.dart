import 'package:get/get.dart';

import '../controllers/dailydelivery_controller.dart';

class DailydeliveryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DailydeliveryController>(
      () => DailydeliveryController(),
    );
  }
}
