import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/contactus_controller.dart';

class ContactusView extends GetView<ContactusController> {
  const ContactusView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Align(
          alignment:Alignment.topCenter,
          child: Container(
            width: 300,
            height: 300,
            margin:EdgeInsets.only(top:100),
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3),
                ),
              ],
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                 Text(
                      'Our Store Address:         ',
                      style: TextStyle(
                        fontSize: 22,
                        color: Colors.green,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height:5),
                Row(
                  children: [
                    Icon(Icons.location_on, color: Colors.black),
                    SizedBox(width: 7),
                    Text(' Plot No.1017, ', style: TextStyle(fontSize: 18)),
                    
                  
                
                  ],
                ),
              
                
                Text('   Road no.2/6,                            ', style: TextStyle(fontSize: 18)),
               
  Container(
child: Padding(
  padding: EdgeInsets.all(7),
  child: Align(
  alignment: Alignment.center,
  child: 
  Text('  Miyapur,                                   ',style: TextStyle(fontSize: 18)),
),
  )    ),        
              //  Text('Miyapur, ',style: TextStyle(fontSize: 16)),
                Text('   Hyderabad-500049.                  ',style: TextStyle(fontSize: 18)),

                Text('  Mathrusri Nagar                       ',style: TextStyle(fontSize: 18)),
                Row(
                  children: [
                    Icon(Icons.phone, color: Colors.black),
                    SizedBox(width: 7),
                    Text(' +91 7032909949',style: TextStyle(fontSize: 18)),
                  ],
                ),
                Row(
                  children: [
                    Icon(Icons.email, color: Colors.black),
                    SizedBox(width: 7),
                    Text(' admin@dhenusya.com',style: TextStyle(fontSize: 18)),
                  ],
                ),
              ],
            ),
          ),
        ),
    );
    
  }
}

       