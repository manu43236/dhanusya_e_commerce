import 'package:get/get.dart';

import '../controllers/mobilenumber_controller.dart';

class MobilenumberBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MobilenumberController>(
      () => MobilenumberController(),
    );
  }
}
