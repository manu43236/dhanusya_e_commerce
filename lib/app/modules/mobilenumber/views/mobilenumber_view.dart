import 'package:dhenusya_e_commerce/app/modules/login/controllers/login_controller.dart';
import 'package:dhenusya_e_commerce/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';

import '../controllers/mobilenumber_controller.dart';

class MobilenumberView extends GetView<MobilenumberController> {
   MobilenumberView({Key? key}) : super(key: key);

  // final TextEditingController mobilecontroller = TextEditingController();
  // bool isValidMobilenumber = true;

  // List<String> registeredMobilenumber = [];

  // bool isRegisteredMobilenumber(String constMobilenumber) {
  //   return registeredMobilenumber.contains(MobilenumberView);
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Align(
          alignment: Alignment.topCenter,
          child: Padding(
            padding: const EdgeInsets.only(top: 50.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.asset('assets/images/dhenusya_logo.png', width: 150),
                const SizedBox(height: 20),
                const Text(
                  'Dhenusya Organics',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.green, fontSize: 35),
                ),
                const SizedBox(height: 50),
                Image.asset('assets/images/phone_logo.png', width: 60),
                const SizedBox(height: 30),
                const Text(
                  'Enter Your Mobile Number',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 25),
                ),
                const SizedBox(height: 15),
                Container(
                  width: 300,
                  height: 80,
                  child: TextField(
                    keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.digitsOnly,
                      LengthLimitingTextInputFormatter(10)
                    ],
                    // controller: mobilecontroller,
                    // onChanged: (value) {
                    //   (() {
                    //     isValidMobilenumber = value.length == 10;
                    //   });
                    //   // Further validation here if required
                    // },
                    decoration: const InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      hintText: 'Mobile Number',
                      // errorText:
                      //     _isValid ? null : 'Enter a valid Number',
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 25.0),
                InkWell(
                  onTap: () {
                    // if (isValidMobilenumber) {
                    //   if (isRegisteredMobilenumber(mobilecontroller.text)) {
                    //     Get.toNamed(Routes.HOMEPAGE);
                    //   } else {
                    //     // Display the message that the number isn't registered
                    //     ScaffoldMessenger.of(context).showSnackBar(
                    //         const SnackBar(
                    //             content: Text(
                    //                 'This mobile number is not registered.')));
                    //   }
                    // } else {
                    //   // Optionally: show an error message if the input is not valid
                    //   ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    //       content: Text(
                    //           'Please enter a valid 10-digit mobile number.')));
                    // }
                    
                       Get.toNamed(Routes.MOBILEOTP);
                  },
                  child: Container(
                    padding: const EdgeInsets.all(12.0),
                    margin: const EdgeInsets.symmetric(horizontal: 36.0),
                    decoration: const BoxDecoration(
                      color: Colors.grey,
                      borderRadius:
                          BorderRadius.all(Radius.elliptical(100.0, 150.0)),
                    ),
                    child: const Text(
                      'Submit',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 30,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 100),
                TextButton(
                    onPressed: () {
                      Get.toNamed(Routes.LOGIN);
                      // Get.toNamed(Routes.MOBILEOTP);
                    },
                    child: const Text(
                      'Go Back',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 25,
                      ),
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
      
  
