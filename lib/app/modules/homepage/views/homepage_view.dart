import 'package:dhenusya_e_commerce/app/routes/app_pages.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/homepage_controller.dart';

// class HomepageView extends GetView<HomepageController> {
//   const HomepageView({Key? key}) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
      
      
//     );
//   }
// }

class HomepageView extends StatefulWidget {
  @override
  State<HomepageView> createState() => _TabBarExampleState();
}

class _TabBarExampleState extends State<HomepageView>
    with TickerProviderStateMixin {
  late final TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        
        title: Text(
          '      Dhenusya Organics',
          style: TextStyle(
            fontSize: 20,
            
              color: Color(0xFF309947),



            fontWeight: FontWeight.bold,
          ),
        ),
        leading: IconButton(
          icon: Icon(Icons.menu),
          color: Colors.green,

          onPressed: () {
            // Handle drawer button tap
          },
        ),
       backgroundColor: Color(0xFFFFFAB3),
 // Set the background color
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.notifications),
            color: Color(0xFF309947),

            onPressed: () {
              // Handle notification button tap
            },
          ),
          IconButton(
            icon: Icon(Icons.shopping_cart),
            color: Color(0xFF309947),

            onPressed: () {
              // Handle add to cart button tap
// Navigator.push(
//       context,
//       MaterialPageRoute(builder: (context) => Cartpage()),

// );
              
            },
          ),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          
          children: [
          //  SizedBox(height:80),
            DrawerHeader(
                 decoration: BoxDecoration(
                  color: Colors.green,
                 ),
                 child:Container(
                  height:20,
                  child:Row (
                    children:[
                      Icon(
                        Icons.person_2_rounded,
                      color:Color(0xFFE7E6E6),
                    ),
          SizedBox(width:8),
                Text(
                    'Hi, Pujitha',
                    style: TextStyle( fontSize: 21,
                      color: Colors.white,
                    ),
                  ),
                    ],
              ),
            ),
            ),
            SizedBox(height: 1),
            ListTile(
  dense: true,
  contentPadding: const EdgeInsets.symmetric(horizontal:16),
  title: Padding(
    padding: EdgeInsets.only(right: 8.5), // Adjust the padding as needed
   child: Row(
  
  children: [
    Text('Your Account   ',style: TextStyle(fontSize: 17, color: Color(0xFF309947),
)),
    
    Icon(Icons.account_circle,color: Color(0xFF309947),
),
  ],
),
  ),
  onTap: () {
    // Handle item 1 tap
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => YourAccount()),
    //             );
  },
),

            SizedBox(height: 1),
            ListTile(
              dense: true,
              title: Text(
                'Your Subscription',
                style: TextStyle(
                  fontSize: 17,
                  color:Color(0xFF309947),
                ),
              ),
              onTap: () {
                // Handle item 2 tap
      //           Navigator.push(
      // context,
      // MaterialPageRoute(builder: (context) => Subscription()),
      //           );
      Get.toNamed(Routes.SUBSCRIPTION);
              },
            ),
            ListTile(
              dense: true,
              title: Text(
                'Your Cart Orders',
                style: TextStyle(
                  fontSize: 17,
                  color: Color(0xFF309947),
                ),
              ),
               onTap: () {
                // Handle item 2 tap
      //           Navigator.push(
      // context,
      // MaterialPageRoute(builder: (context) => Cartorders()),
      //           );
      Get.toNamed(Routes.CARTORDERS);
              },
            ),
      //       ListTile(
      //         dense: true,
      //         title: Text(
      //           'Order Status',
      //           style: TextStyle(
      //             fontSize: 17,
      //             color: Color(0xFF309947),
      //           ),
      //         ),
      //         onTap: () {
      //           // Handle item 4 tap
      // //           Navigator.push(
      // // context,
      // // MaterialPageRoute(builder: (context) => Orderplaced()),
      // //           );
      //         },
      //       ),
            ListTile(
              dense: true,
              title:  const Text(
                '_____________________',
                style: TextStyle(
                  fontSize: 17,
                  color: const Color.fromARGB(255, 166, 167, 168),
                ),
              ),
              onTap: () {
                // Handle item 5 tap
              },
            ),
            
             ListTile(
  dense: true,
  contentPadding: const EdgeInsets.symmetric(horizontal:16),
  title: Padding(
    padding: EdgeInsets.only(right: 8.5), // Adjust the padding as needed
   child: Row(
  
  children: [
    Text('Daily Delivery  ',style: TextStyle(fontSize: 17, color: Color(0xFF309947),
)),
    
    Icon(Icons.local_shipping,color: Color(0xFF309947),
),
  ],
),
  ),
  onTap: () {
    // Handle item 1 tap
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => Delivery()),
    // );
    Get.toNamed(Routes.DAILYDELIVERY);
  },
),

            ListTile(
              dense: true,
              title:  const Text(
                'Offers Zone',
                style: TextStyle(
                  fontSize: 17,
                  color: Color(0xFF309947),
                ),
              ),
              onTap: () {
                // Handle item 7 tap

      //           Navigator.push(
      // context,
      // MaterialPageRoute(builder: (context) => List()),
      //           );
      Get.toNamed(Routes.OFFERSZONE);
              },
            ),
            ListTile(
              dense: true,
              title:  const Text(
                '_____________________',
                style: TextStyle(
                  fontSize: 18,
                  color: const Color.fromARGB(255, 166, 167, 168),
                ),
              ),
              onTap: () {
                // Handle item 8 tap
              },
            ),
             ListTile(
  dense: true,
  contentPadding: const EdgeInsets.symmetric(horizontal:16),
  title: Padding(
    padding: EdgeInsets.only(right: 8.5), // Adjust the padding as needed
   child: Row(
  
  children: [
    Text('Contact Us   ',style: TextStyle(fontSize: 17, color: Color(0xFF309947),
)),
    
    Icon(Icons.phone,color: Color(0xFF309947),
),
  ],
),
  ),
  
    // Handle item 1 tap
    onTap: () {
                // Handle item 2 tap
      //           Navigator.push(
      // context,
      // MaterialPageRoute(builder: (context) => Contact()),
      //           );
              
            Get.toNamed(Routes.CONTACTUS);
  },
),
ListTile(
  dense: true,
  contentPadding: const EdgeInsets.symmetric(horizontal:16),
  title: Padding(
    padding: EdgeInsets.only(right: 8.5), // Adjust the padding as needed
   child: Row(
  
  children: [
    Text('Email Us   ',style: TextStyle(fontSize: 17, color: Color(0xFF309947),
)),
    
    Icon(Icons.email,color: Color(0xFF309947),
),
  ],
),
  ),
  onTap: () {
    // Handle item 1 tap

    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => email()),
    //             );

    Get.toNamed(Routes.EMAILUS);
  },
),
            
            ListTile(
              dense: true,
              title: const Text(
                '_____________________',
                style: TextStyle(
                  fontSize: 18,
                  color: const Color.fromARGB(255, 166, 167, 168),
                ),
              ),
              onTap: () {
                // Handle item 11 tap
              },
            ),
            ListTile(
              dense: true,
              title:  const Text(
                'Help!',
                style: TextStyle(
                  fontSize: 17,
                  color: Color(0xFF309947),
                ),
              ),
              onTap: () {
                // Handle item 12 tap

      //           Navigator.push(
      // context,
      // MaterialPageRoute(builder: (context) => help()),
      //           );
              
               Get.toNamed(Routes.HELP);
              },
            ),
            ListTile(
              dense: true,
              title:const  Text(
                'Sign Out!',
                style: TextStyle(
                  fontSize: 17,
                  color: Color(0xFF309947),
                ),
              ),
              onTap: () {
                // Handle item 13 tap

      //           Navigator.push(
      // context,
      // MaterialPageRoute(builder: (context) => Welcome()),
      //           );
              },
            ),
            // Add more ListTile items here
          ],
        ),
      ),
      body: Column(
        children: [
          Container(
            color: Color(0xFFFFFAB3)
,
            child: Column(
              children: [
                // Text(
                //   'Dhenusya Organics',
                //   style: TextStyle(
                //     fontSize: 20,
                //     fontWeight: FontWeight.bold,
                //   ),
                // ),

                SizedBox(
                  width: 450,
                  height: 40,
                  child: SearchBar(
                    hintText: 'Search Here',
                    onChanged: (query) {
                      // Perform the search here.
                    },
                  ),
                ),
                SizedBox(height: 20),
                
                Container(
                  color: Color(0xFFFFFAB3),

                  child: TabBar(
                    controller: _tabController,
                    tabs: <Widget>[
                      CircularTab(
                        imageAsset: 'assets/images/vegetables.jpg',
                        title: "Vegetables",
                         
                      ),

//                       GestureDetector(
//   onTap: () {
//     // Navigate to the next page when "Vegetables" tab is clicked
//     Navigator.push(
//       context,
//       MaterialPageRoute(builder: (context) => items()), // Replace "VegetablesPage" with the actual page you want to navigate to
//     );
//   },
//   child: CircularTab(
//     imageAsset: 'assets/vegetables.jpg',
//     title: "Vegetables",
//   ),
// ),
                      
                      CircularTab(
                        imageAsset: 'assets/images/dairy.jpg',
                        title: "Dairy",
                        
                      ),
                      CircularTab(
                        imageAsset: 'assets/images/meat.jpg',
                        title: "Meat",
                      ),
                      // CircularTab(
                      //   imageAsset: 'assets/images/mix-dry-fruits.jpg',
                      //   title: "Dry Fruits",
                      // ),



                      
                      

                    ],
                  ),
                ),

               


// SingleChildScrollView(
//                   scrollDirection: Axis.horizontal,
//                   child: Container(
//                     color: Color(0xFFFFFAB3),
//                     child: TabBar(
//                       controller: _tabController,
//                       tabs: <Widget>[
//                         CircularTab(
//                           imageAsset: 'assets/vegetables.jpg',
//                           title: "Vegetables",
//                         ),
//                         CircularTab(
//                           imageAsset: 'assets/dairy.jpg',
//                           title: "Dairy",
//                         ),
//                         CircularTab(
//                           imageAsset: 'assets/meat.jpg',
//                           title: "Meat",
//                         ),
//                         CircularTab(
//                           imageAsset: 'assets/mix-dry-fruits.jpg',
//                           title: "Dry Fruits",
//                         ),

//                       ],
//                     ),
//                   ),
//                 ),



             ],
            ), 
          ),
          Container(
            color: Colors.white,
            height:400,
            width:450,

            // child: Expanded(
            //   child: TabBarView(
            //     controller: _tabController,
            //     children: <Widget>[
            //       Center(
            //         child: Text("It's cloudy here"),
            //       ),
            //       Center(
            //         child: Text("It's rainy here"),
            //       ),
            //       Center(
            //         child: Text("It's sunny here"),
            //       ),
            //     ],
            //   ),
            // ),

           
            
          ),
        ],
      ),
    );
  }
}

class CircularTab extends StatelessWidget {
  final String imageAsset;
  final String title;

  const CircularTab({required this.imageAsset, required this.title});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 60,
          height: 60,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: AssetImage(imageAsset),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Text(title),
      ],
    );
  }
}




   