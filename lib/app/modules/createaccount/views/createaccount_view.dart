import 'package:dhenusya_e_commerce/app/modules/login/controllers/login_controller.dart';
import 'package:dhenusya_e_commerce/app/routes/app_pages.dart';
//import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';



class CreateaccountView extends GetView<LoginController> {
  CreateaccountView({Key? key}) : super(key: key);

  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  bool isValidEmail = true;
  bool isValidPassword = true;

  bool isPasswordHidden = true;

  bool isGmail(String email) {
    final pattern = r'^[a-z].+@gmail\.com$';
    final regex = RegExp(pattern);
    return regex.hasMatch(email);
  }

  bool isPassword(String password) {
    final pattern = r'^(?=.*[A-Z])(?=.*[0-9]).{8,}$';
    final regex = RegExp(pattern);
    return regex.hasMatch(password);
  }

  bool isNameValid(String name) {
    final pattern =
        r'^[a-zA-Z]{1,30}$'; // Allows only letters and a length of 1 to 30
    final regex = RegExp(pattern);
    return regex.hasMatch(name);
  }

  final rePasswordController = TextEditingController();
  bool isPasswordsMatch = true;

  bool isFirstNameValid = true;
  bool isLastNameValid = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Align(
          alignment: Alignment.topCenter,
          child: Padding(
            padding: const EdgeInsets.only(top: 7.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Image.asset(
                  'assets/images/dhenusya_logo.png',
                  width: 150,
                ),
                //const SizedBox(height: 0),
                const Text(
                  'Dhenusya Organics',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.green,
                    fontSize: 35,
                  ),
                ),
                //const SizedBox(height: 15),
                const Text(
                  'Welcomes You To Our Family',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 27,
                  ),
                ),
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(height: 16.0),
                      Container(
                        width: 350, // 80% of screen width
                        child: TextField(
                          controller: emailController,
                          onChanged: (value) {
                            (() {
                              isValidEmail = isGmail(value);
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            hintText: 'Enter Email-ID',
                            errorText: isValidEmail
                                ? null
                                : 'Enter a valid Email address',
                            prefixIcon: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal:
                                    10.0, // Optional padding to adjust image position
                              ),
                              child: Image.asset(
                                'assets/images/email.png',
                                width: 20,
                                height: 20,
                              ), // Adjust width and height if needed
                            ),
                            enabledBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                            ),
                            focusedBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                            ),
                            border: const OutlineInputBorder(),
                          ),
                        ),
                      ),
                      const SizedBox(height: 13),
                      Container(
                        width: 350, // 80% of screen width
                        child: TextField(
                          maxLength: 30,
                          onChanged: (value) {
                            (() {
                              isFirstNameValid = isNameValid(value);
                            });
                          },
                          decoration: InputDecoration(
                            counterText: " ",
                            filled: true,
                            fillColor: Colors.white,
                            hintText: 'Enter First Name',
                            errorText: isFirstNameValid
                                ? null
                                : 'Enter a valid name (1-30 letters only)',
                            prefixIcon: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal:
                                    10.0, // Optional padding to adjust image position
                              ),
                              child: Image.asset(
                                'assets/images/account_logo.png',
                                width: 20,
                                height: 20,
                              ), // Adjust width and height if needed
                            ),
                            enabledBorder: const OutlineInputBorder(
                              //borderRadius: BorderRadius.circular(500),
                              borderSide: BorderSide(color: Colors.black),
                            ),
                            focusedBorder: const OutlineInputBorder(
                              //borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(color: Colors.black),
                            ),
                          ),
                          // textAlign: TextAlign.center,
                          // cursorColor: Color.fromARGB(255, 32, 30, 30),
                        ),
                      ),
                      const SizedBox(height: 0),
                      Container(
                        width: 350, // 80% of screen width
                        child: TextField(
                          maxLength: 30,
                          onChanged: (value) {
                            (() {
                              isLastNameValid = isNameValid(value);
                            });
                          },

                          decoration: InputDecoration(
                            //counterText: "",
                            filled: true,
                            fillColor: Colors.white,
                            hintText: 'Enter Last Name',
                            errorText: isLastNameValid
                                ? null
                                : 'Enter a valid name (1-30 letters only)',
                            prefixIcon: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal:
                                    10.0, // Optional padding to adjust image position
                              ),
                              child: Image.asset(
                                'assets/images/account_logo.png',
                                width: 20,
                                height: 20,
                              ), // Adjust width and height if needed
                            ),
                            enabledBorder: const OutlineInputBorder(
                              //borderRadius: BorderRadius.circular(500),
                              borderSide: BorderSide(color: Colors.black),
                            ),
                            focusedBorder: const OutlineInputBorder(
                              //borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(color: Colors.black),
                            ),
                          ),
                          // textAlign: TextAlign.center,
                          // cursorColor: Color.fromARGB(255, 32, 30, 30),
                        ),
                      ),
                      const SizedBox(height: 1.0),
                      Container(
                        width: 350, // 80% of screen width
                        child: TextField(
                          controller: passwordController,
                          onChanged: (value) {
                            (() {
                              isValidPassword = isPassword(value);
                            });
                          },
                          obscureText: isPasswordHidden, // to hide password
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            hintText: 'Enter Password',
                            enabledBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                            ),
                            focusedBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                            ),
                            prefixIcon: const Icon(
                              Icons.lock,
                              color: Colors.green,
                            ),
                            suffixIcon: IconButton(
                              icon: Icon(
                                isPasswordHidden
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: const Color.fromARGB(255, 50, 128,
                                    10), // Fixed the invalid RGB value here
                              ),
                              padding: const EdgeInsets.only(right: 10),
                              color: const Color.fromARGB(255, 50, 228, 10),
                              onPressed: () {
                                (() {
                                  isPasswordHidden = !isPasswordHidden;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 13),
                      Container(
                        width: 350, // 80% of screen width
                        child: TextField(
                          controller: rePasswordController,
                          obscureText: true, // to hide password
                          onChanged: (value) {
                            (() {
                              isPasswordsMatch =
                                  passwordController.text == value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            hintText: 'Enter Re-Password',
                            errorText: isPasswordsMatch
                                ? null
                                : 'Passwords do not match',
                            enabledBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                            ),
                            focusedBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                            ),
                            border: const OutlineInputBorder(),
                            prefixIcon: const Icon(
                              Icons.lock,
                              color: Colors.green,
                            ),
                            suffixIcon: IconButton(
                              icon: Icon(
                                isPasswordHidden
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: const Color.fromARGB(255, 50, 128, 10),
                              ),
                              padding: const EdgeInsets.only(right: 10),
                              color: const Color.fromARGB(255, 50, 228, 10),
                              onPressed: () {
                                (() {
                                  isPasswordHidden = !isPasswordHidden;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 13),
                      Container(
                        width: 350, // 80% of screen width
                        child: TextField(
                          keyboardType: TextInputType
                              .number, // This shows the numeric keyboard
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly,
                            LengthLimitingTextInputFormatter(10)
                          ], // This ensures that only digits can be entered
                          onChanged: (value) {
                            // You can add further validation here if required
                          },

                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            hintText: 'Enter Mobile Number',
                            prefixIcon: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal:
                                    10.0, // Optional padding to adjust image position
                              ),
                              child: Image.asset(
                                'assets/images/phone_logo.png',
                                width: 20,
                                height: 20,
                              ), // Adjust width and height if needed
                            ),
                            enabledBorder: const OutlineInputBorder(
                              //borderRadius: BorderRadius.circular(500),
                              borderSide: BorderSide(color: Colors.black),
                            ),
                            focusedBorder: const OutlineInputBorder(
                              //borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(color: Colors.black),
                            ),
                          ),
                          // textAlign: TextAlign.center,
                          // cursorColor: Color.fromARGB(255, 32, 30, 30),
                        ),
                      ),
                      SizedBox(height: 8),
                      const Text(
                          'By creating an account, you agree to our\nTerms of Service and Privacy Policy',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                          )),
                      const SizedBox(height: 20.0),
                      InkWell(
                        onTap: () {
                          Get.toNamed(Routes.HOME);
                        },
                        child: Container(
                          //width: Get.width,
                          padding: const EdgeInsets.all(12.0),
                          margin: const EdgeInsets.symmetric(horizontal: 36.0),
                          decoration: const BoxDecoration(
                            color: Colors.grey,
                            borderRadius: BorderRadius.all(
                                Radius.elliptical(100.0, 150.0)),
                          ),
                          child: const Text(
                            'Get Started',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    rePasswordController.dispose();
    //super.dispose();
  }
}
