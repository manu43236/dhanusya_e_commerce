import 'package:get/get.dart';

import '../controllers/offerszone_controller.dart';

class OfferszoneBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OfferszoneController>(
      () => OfferszoneController(),
    );
  }
}
