import 'package:dhenusya_e_commerce/app/routes/app_pages.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var style;
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const SizedBox(height: 80),
          Image.asset(
            'assets/images/dhenusya_logo.png',
            width: 200,
            //height: 100,
          ),
          //const SizedBox(height: 5),
          const Align(
            alignment: Alignment.topCenter,
            child: Text(
              'Dhenusya Organics',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.green,
                fontSize: 35,
                //height: -0.5,
              ),
            ),
          ),
          // const SizedBox(height: 15),
          const SizedBox(height: 20), // Add spacing
          Container(
            width: 350,
            height: 50,
            child: OutlinedButton(
              style: style,
              onPressed: () {
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) => const Googlesignup()),
                // );
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/images/google_logo.png',
                    width: 45,
                    height: 25,
                  ),
                  const SizedBox(width: 10),
                  // Adjusted width to provide spacing
                  const Text('Sign up with Google',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 25,
                      )),
                ],
              ),
            ),
          ),
          //),
          const SizedBox(height: 0),
          const SizedBox(height: 20), // Add spacing
          Container(
            width: 350,
            height: 50,
            child: OutlinedButton(
              style: style,
              onPressed: () {
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) => const applesignup()),
                // );
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/images/apple_logo.png',
                    width: 45,
                    height: 25,
                  ),
                  const SizedBox(
                      width: 10), // Adjusted width to provide spacing
                  const Text('Sign up with AppleId',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 25,
                      )),
                ],
              ),
            ),
          ),
          const SizedBox(height: 0),
          const SizedBox(height: 20), // Add spacing
          Container(
            width: 350,
            height: 50,
            child: OutlinedButton(
              style: style,
              onPressed: () {
                Get.toNamed(Routes.CREATEACCOUNT);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/images/account_logo.png',
                    width: 45,
                    height: 25,
                  ),
                  const SizedBox(
                      width: 50), // Adjusted width to provide spacing
                  const Text('Create Account',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 25,
                      )),
                ],
              ),
            ),
          ),
          const SizedBox(height: 20),
          const Text(
            '______________OR______________   ',
            style: TextStyle(
              fontSize: 21,
              color: Color.fromARGB(255, 78, 78, 78),
            ),
          ),
          // const SizedBox(height: 20),
          const SizedBox(height: 20), // Add spacing
          Container(
            width: 350,
            height: 50,
            child: OutlinedButton(
              style: style,
              onPressed: () {
                 Get.toNamed(Routes.MOBILENUMBER);
                
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/images/phone_logo.png',
                    width: 45,
                    height: 25,
                  ),
                  const SizedBox(
                      width: 10), // Adjusted width to provide spacing
                  const Text('Use Mobile Number',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 25,
                      )),
                ],
              ),
            ),
          ),
          const SizedBox(height: 25),
          RichText(
              text: TextSpan(
                  text: 'Already have an account?',
                  style: const TextStyle(
                    fontSize: 18,
                    color: Colors.black,
                  ),
                  children: <TextSpan>[
                TextSpan(
                  text: 'Sign In',
                  style: const TextStyle(
                    fontSize: 25,
                    color: Colors.green,
                  ),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      // Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) =>
                      //             const Signin())); // Ensure that 'signin' is the correct name of your Widget.
                    },
                )
              ]))
        ],
      ),
    );
  }
}
