import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/cartorders_controller.dart';

class CartordersView extends GetView<CartordersController> {
  const CartordersView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('CartordersView'),
      //   centerTitle: true,
      // ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 60),
          child: ListView(
            children: <Widget>[
              Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Image.asset(
                      'assets/images/dhenusya_logo.png',
                      height: 150,
                      width: 150,
                      fit: BoxFit.cover,
                    ),
                    const SizedBox(height: 10),
                    const Text(
                      'Dhenusya Organics',
                      style: TextStyle(
                        fontSize: 31,
                        color: Color.fromRGBO(12, 96, 30, 1),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 20),
                    const Text(
                      'Welcome ,',
                      style: TextStyle(
                        fontSize: 25,
                        color: Color.fromRGBO(12, 96, 30, 1),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 120),
                    const Text(
                      'You do not have any order History!',
                      style: TextStyle(
                        fontSize: 24,
                        color: Color.fromRGBO(12, 96, 30, 1),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 110),
                    ElevatedButton(
                      onPressed: () {
                        // Handle navigation to the next page
                      },
                      style: ElevatedButton.styleFrom(
                        primary: Colors.orange,
                        minimumSize: Size(220, 41),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(13.0),
                        ),
                      ),
                      child: const Text(
                        'Continue Shopping',
                        style: TextStyle(
                          fontSize: 18,
                          color: Color.fromARGB(255, 28, 15, 15),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

void main() {
  runApp(CartordersView());
}
