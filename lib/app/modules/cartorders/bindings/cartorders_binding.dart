import 'package:get/get.dart';

import '../controllers/cartorders_controller.dart';

class CartordersBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CartordersController>(
      () => CartordersController(),
    );
  }
}
