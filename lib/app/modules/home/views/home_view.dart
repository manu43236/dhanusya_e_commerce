import 'package:dhenusya_e_commerce/app/routes/app_pages.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   // title: const Text('HomeView'),
      //   centerTitle: true,
      // ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/dhenusya_logo.png',  
              width: 700,
              height: 300,
            ),
            const SizedBox(height: 20),
            const Align(
              alignment: Alignment.center,
              child: Text(
                'Welcome To\nDhenusya Organics',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color.fromARGB(255, 71, 228, 76),
                  fontSize: 35,
                ),
              ),
            ),
            const SizedBox(height: 30),
            const Text(
              'A Quick tour',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
              ),
            ),
            const SizedBox(height: 20),
            GestureDetector(
              onTap: () {
                Get.toNamed(Routes.LOGIN);
                // Get.toNamed(Routes.WALLET);
                // Get.toNamed(Routes.HOMEPAGE);
              },
              child: Image.asset(
                'assets/images/arrow.png',
                width: 70,
                height: 30,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
