import 'package:get/get.dart';

import '../modules/Offerszone/bindings/offerszone_binding.dart';
import '../modules/Offerszone/views/offerszone_view.dart';
import '../modules/cartorders/bindings/cartorders_binding.dart';
import '../modules/cartorders/views/cartorders_view.dart';
import '../modules/contactus/bindings/contactus_binding.dart';
import '../modules/contactus/views/contactus_view.dart';
import '../modules/createaccount/bindings/createaccount_binding.dart';
import '../modules/createaccount/views/createaccount_view.dart';
import '../modules/dailydelivery/bindings/dailydelivery_binding.dart';
import '../modules/dailydelivery/views/dailydelivery_view.dart';
import '../modules/emailus/bindings/emailus_binding.dart';
import '../modules/emailus/views/emailus_view.dart';
import '../modules/help/bindings/help_binding.dart';
import '../modules/help/views/help_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/homepage/bindings/homepage_binding.dart';
import '../modules/homepage/views/homepage_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/mobilenumber/bindings/mobilenumber_binding.dart';
import '../modules/mobilenumber/views/mobilenumber_view.dart';
import '../modules/mobileotp/bindings/mobileotp_binding.dart';
import '../modules/mobileotp/views/mobileotp_view.dart';
import '../modules/splash/bindings/splash_binding.dart';
import '../modules/splash/views/splash_view.dart';
import '../modules/subscription/bindings/subscription_binding.dart';
import '../modules/subscription/views/subscription_view.dart';
import '../modules/user_register/bindings/user_register_binding.dart';
import '../modules/user_register/views/user_register_view.dart';
import '../modules/wallet/bindings/wallet_binding.dart';
import '../modules/wallet/views/wallet_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.USER_REGISTER,
      page: () => const UserRegisterView(),
      binding: UserRegisterBinding(),
    ),
    GetPage(
      name: _Paths.SPLASH,
      page: () => const SplashView(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => const LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.CREATEACCOUNT,
      page: () =>  CreateaccountView(),
      binding: CreateaccountBinding(),
    ),
    GetPage(
      name: _Paths.MOBILENUMBER,
      page: () =>  MobilenumberView(),
      binding: MobilenumberBinding(),
    ),
    GetPage(
      name: _Paths.HOMEPAGE,
      page: () => HomepageView(),
      binding: HomepageBinding(),
    ),
    GetPage(
      name: _Paths.WALLET,
      page: () => const WalletView(),
      binding: WalletBinding(),
    ),
    GetPage(
      name: _Paths.CARTORDERS,
      page: () => const CartordersView(),
      binding: CartordersBinding(),
    ),
    GetPage(
      name: _Paths.SUBSCRIPTION,
      page: () => const SubscriptionView(),
      binding: SubscriptionBinding(),
    ),
    GetPage(
      name: _Paths.CONTACTUS,
      page: () => const ContactusView(),
      binding: ContactusBinding(),
    ),
    GetPage(
      name: _Paths.EMAILUS,
      page: () => EmailusView(),
      binding: EmailusBinding(),
    ),
    GetPage(
      name: _Paths.DAILYDELIVERY,
      page: () => const DailydeliveryView(),
      binding: DailydeliveryBinding(),
    ),
    GetPage(
      name: _Paths.OFFERSZONE,
      page: () => const OfferszoneView(),
      binding: OfferszoneBinding(),
    ),
    GetPage(
      name: _Paths.HELP,
      page: () => HelpView(),
      binding: HelpBinding(),
    ),
    GetPage(
      name: _Paths.MOBILEOTP,
      page: () => const MobileotpView(),
      binding: MobileotpBinding(),
    ),
  ];
}

