part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const USER_REGISTER = _Paths.USER_REGISTER;
  static const SPLASH = _Paths.SPLASH;
  static const HOME = _Paths.HOME;
  static const LOGIN = _Paths.LOGIN;
  static const CREATEACCOUNT = _Paths.CREATEACCOUNT;
  static const MOBILENUMBER = _Paths.MOBILENUMBER;
  static const HOMEPAGE = _Paths.HOMEPAGE;
  static const WALLET = _Paths.WALLET;
  static const CARTORDERS = _Paths.CARTORDERS;
  static const SUBSCRIPTION = _Paths.SUBSCRIPTION;
  static const CONTACTUS = _Paths.CONTACTUS;
  static const EMAILUS = _Paths.EMAILUS;
  static const DAILYDELIVERY = _Paths.DAILYDELIVERY;
  static const OFFERSZONE = _Paths.OFFERSZONE;
  static const HELP = _Paths.HELP;
  static const MOBILEOTP = _Paths.MOBILEOTP;
}

abstract class _Paths {
  _Paths._();
  static const USER_REGISTER = '/user-register';
  static const SPLASH = '/splash';
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const CREATEACCOUNT = '/createaccount';
  static const MOBILENUMBER = '/mobilenumber';
  static const HOMEPAGE = '/homepage';
  static const WALLET = '/wallet';
  static const CARTORDERS = '/cartorders';
  static const SUBSCRIPTION = '/subscription';
  static const CONTACTUS = '/contactus';
  static const EMAILUS = '/emailus';
  static const DAILYDELIVERY = '/dailydelivery';
  static const OFFERSZONE = '/offerszone';
  static const HELP = '/help';
  static const MOBILEOTP = '/mobileotp';
}
