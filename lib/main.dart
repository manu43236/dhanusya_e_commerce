import 'package:dhenusya_e_commerce/app/modules/splash/bindings/splash_binding.dart';
import 'package:dhenusya_e_commerce/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';

void main() {
  runApp(
    GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      locale: const Locale('en', 'US'),
      title: "Dhenusya",
      initialBinding: SplashBinding(),
    ),
  );
}
